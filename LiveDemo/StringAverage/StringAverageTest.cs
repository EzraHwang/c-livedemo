using NUnit.Framework;

namespace LiveDemo.StringAverage
{
    public class StringAverageTest
    {
        private StringAverage _stringAverage;

        [SetUp]
        public void Setup()
        {
            _stringAverage = new StringAverage();
        }

        [Test]
        [TestCase("four", "zero nine five two")]
        [TestCase("three", "four six two three")]
        [TestCase("three", "one two three four five")]
        [TestCase("four", "five four")]
        [TestCase("zero", "zero zero zero zero zero")]
        [TestCase("two", "one one eight one")]
        [TestCase("n/a", "")]
        [TestCase("n/a", "twenty")]
        [TestCase("n/a", "fivefour two")]
        public void SimpleTest(string expected, string testInput)
        {
            var actual = _stringAverage.Computed(testInput);
            //var actual = testInput.Average();
            
            Assert.AreEqual(expected, actual);
        }
    }
}